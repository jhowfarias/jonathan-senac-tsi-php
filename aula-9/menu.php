<?php 
        if (session_status() !== PHP_SESSION_ACTIVE){
            session_start();
        }

        if(!isset($_SESSION['id_usuario'])){
            header('Location: ../login.php');
            exit();
        }

echo "<a href='usuario/'>Usuario</a>
<a href='login.php'>Sair</a>";