<?php
echo "<pre>";

// Constante no PHP 

define ('QTD_PAGINAS', 10 );

echo "Valor da minha constrante é: " . QTD_PAGINAS;

// variavel para passar valor para uma constante 

$ip_do_banco = '192.168.45.12';

define ('IP_DO_BANCO', $ip_do_banco);

echo "\nO IP DO SGDB É: " . IP_DO_BANCO;

//Constante Magica

echo "\nEstou na linha: " . __LINE__;
echo "\nAgora estou na linha: " . __LINE__;
echo "\nEste é o arquivo: " . __FILE__;


// Depurar o Codigo
echo "\n";
var_dump($ip_do_banco);

// Vetor Array 

echo "\n\n";

$dias_da_semana = ['dom','seg','ter','qua','qui','sex','sab'];

unset ($dias_da_semana); //destroi a variavel

$dias_da_semana [0] = 'dom';
$dias_da_semana [1] = 'seg';
$dias_da_semana [2] = 'ter';
$dias_da_semana [3] = 'qua';
$dias_da_semana [4] = 'qui';
$dias_da_semana [5] = 'sex';
$dias_da_semana [6] = 'sab';

var_dump ($dias_da_semana);

unset ($dias_da_semana);

$dias_da_semana = array (0 => 'dom', 1 => 'seg', 2 => 'ter', 3 => 'qua', 4 => 'qui', 5 => 'sex', 6 => 'sab');

echo "\n";

var_dump ($dias_da_semana);

















echo "</pre>";

