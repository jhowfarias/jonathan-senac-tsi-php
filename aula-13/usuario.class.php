<?php

class usuario {
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $db;

    public function __construct(){
        $this->db = new mysqli('localhost', 'root', '','aula5_php', 3307);
    }

    public function setId (int $id){
        $this->id = $id;
    }
    public function setNome (string $nome){
        $this->nome = $nome;
    }
    public function setEmail (string $email){
        $this->email = $email;
    }
    public function setSenha(string $senha){
        $this->senha = $senha;
    }

    public function getId (int $id): int{
        return $this->id;
    }
    public function getNome (string $nome): string{
       return $this->nome;
    }
    public function getEmail (string $email): string{
       return $this->email;
    }
    public function getSenha(string $senha): string{
        return $this->senha;
    }

    public function saveUser(){
        $this->senha = password_hash($this->senha, PASSWORD_DEFAULT);

        // preparando a consulta para evitar injection 
        //realiza o insert no banco de dados  
        
        $objStmt = $this->db->prepare('REPLACE INTO usuario(id,nome,email,senha) VALUES (?,?,?,?)');

        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);

        if($objStmt->execute()){
            return true; 
        }else {
            return false;
        }      
    }  

    public function __destruct() {
       unset($this->db);
        
    }
/*
    public function listUser() {
            $objStmt = $this->db->prepare('
                                        SELECT
                                            nome, email
                                        FROM
                                            usuario
                                        WHERE
                                            id = ?');
            $objStmt->bind_param('i', $this->id);
            $objStmt->execute();
            $objResult = $objStmt->get_result();
            return $objResult->fetch_assoc();
    
        
    }

    public function delUser(int $id){
       
        // preparando a consulta para deletar
        $sql = $this->db->prepare('DELETE FROM bitbucket WHERE nome = ?');
        $sql->bind_param ('i', $this->id);
        if ($sql->execute()){
            echo "apagado com sucesso";
        } else {
            echo "erro";
        }     
       
      
    }
*/
}
